# Hoja de referencia del paquete sf

Es una traducción no oficial de la hoja de referencia de este paquete realizada por Ryan Garnett, disponible en [rstudio/cheatseets](https://github.com/rstudio/cheatsheets/blob/master/sf.pdf). El idioma original de esta hoja de referencia es Inglés.